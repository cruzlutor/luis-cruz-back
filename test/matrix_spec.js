const should = require('should')
const mocha = require('mocha')
const Matrix = require('./../lib/matrix')


describe('Testing Matrix', function() {


  describe('Build Matrix', function() {
    const matrix = new Matrix()

    it('Should be an array on N equals to 4', function(){
      matrix.build(4).should.be.instanceof(Array)
    })

    it('Should throw err on N equals to 0', function(){
      should( () => matrix.build(0)).throw('N must be between 0 and 100')
    })

    it('Should throw err on N equals to 200', function(){
      should( () => matrix.build(200)).throw('N must be between 0 and 100')
    })
  })


  describe('Update Matrix', function() {
    const matrix = new Matrix()
    matrix.build(100)

    it('Should be an array on X equals to 50', function(){
      matrix.update(50,1,1,10).should.be.instanceof(Array)
    })

    it('Should throw err on X equals to 0', function(){
      should( () => matrix.update(0,1,1,10)).throw('X must be between 0 and N')
    })

    it('Should throw err on X equals to 200', function(){
      should( () => matrix.update(200,1,1,10)).throw('X must be between 0 and N')
    })

    it('Should be an array on Y equals to 50', function(){
      matrix.update(1,50,1,10).should.be.instanceof(Array)
    })

    it('Should throw err on Y equals to 0', function(){
      should( () => matrix.update(1,0,1,10)).throw('Y must be between 0 and N')
    })

    it('Should throw err on Y equals to 200', function(){
      should( () => matrix.update(1,200,1,10)).throw('Y must be between 0 and N')
    })

    it('Should be an array on Z equals to 50', function(){
      matrix.update(1,1,50,10).should.be.instanceof(Array)
    })

    it('Should throw err on Z equals to 0', function(){
      should( () => matrix.update(1,1,0,10)).throw('Z must be between 0 and N')
    })

    it('Should throw err on Z equals to 200', function(){
      should( () => matrix.update(1,1,200,10)).throw('Z must be between 0 and N')
    })

    it('Should be an array on W equals to 5^9', function(){
      matrix.update(1,1,1,Math.pow(5, 9)).should.be.instanceof(Array)
    })

    it('Should throw err on W equals to -11^9', function(){
      should( () => matrix.update(1,1,1, Math.pow(-11, 9))).throw('W must be between -10^9 and 10^9')
    })

    it('Should throw err on W equals to 11^9', function(){
      should( () => matrix.update(1,1,1, Math.pow(11, 9))).throw('W must be between -10^9 and 10^9')
    })

  })

  describe('Query Matrix', function() {
    const matrix = new Matrix()
    matrix.build(4)
    matrix.update(2,2,2,4)


    it('Should throw err on X1 equals to 0', function(){
      should( () => matrix.query(0,1,1,1,1,1)).throw('X1 must be between 0 and N')
    })

    it('Should throw err on X1 equals to 200', function(){
      should( () => matrix.query(200,1,1,1,1,1)).throw('X1 must be between 0 and N')
    })

    it('Should throw err on Y1 equals to 0', function(){
      should( () => matrix.query(1,0,1,1,1,1)).throw('Y1 must be between 0 and N')
    })

    it('Should throw err on Y1 equals to 200', function(){
      should( () => matrix.query(1,200,1,1,1,1)).throw('Y1 must be between 0 and N')
    })

    it('Should throw err on Z1 equals to 0', function(){
      should( () => matrix.query(1,1,0,1,1,1)).throw('Z1 must be between 0 and N')
    })

    it('Should throw err on Z1 equals to 200', function(){
      should( () => matrix.query(1,1,200,1,1,1)).throw('Z1 must be between 0 and N')
    })

    it('Should throw err on X2 equals to 0', function(){
      should( () => matrix.query(1,1,1,0,1,1)).throw('X2 must be between 0 and N')
    })

    it('Should throw err on X2 equals to 200', function(){
      should( () => matrix.query(1,1,1,200,1,1)).throw('X2 must be between 0 and N')
    })

    it('Should throw err on Y2 equals to 0', function(){
      should( () => matrix.query(1,1,1,1,0,1)).throw('Y2 must be between 0 and N')
    })

    it('Should throw err on Y2 equals to 200', function(){
      should( () => matrix.query(1,1,1,1,200,1)).throw('Y2 must be between 0 and N')
    })

    it('Should throw err on Z2 equals to 0', function(){
      should( () => matrix.query(1,1,1,1,1,0)).throw('Z2 must be between 0 and N')
    })

    it('Should throw err on Z2 equals to 200', function(){
      should( () => matrix.query(1,1,1,1,1,200)).throw('Z2 must be between 0 and N')
    })

    it('Should throw err on X2 (1) lower than X1 (2)', function(){
      should( () => matrix.query(2,2,2,1,2,2)).throw('X2 must be higher or equal than X1')
    })

    it('Should throw err on Y2 (1) lower than Y1 (2)', function(){
      should( () => matrix.query(2,2,2,2,1,2)).throw('Y2 must be higher or equal than Y1')
    })

    it('Should throw err on Z2 (1) lower than Z1 (2)', function(){
      should( () => matrix.query(2,2,2,2,2,1)).throw('Z2 must be higher or equal than Z1')
    })

    it('Should pass if X2 (1) equals than X1 (1)', function(){
      matrix.query(1,2,2,1,2,2).should.be.instanceof(Number)
    })

    it('Should pass if Y2 (1) equals than Y1 (1)', function(){
      matrix.query(2,1,2,2,1,2).should.be.instanceof(Number)
    })

    it('Should pass if Z2 (1) equals than Z1 (1)', function(){
      matrix.query(2,2,1,2,2,1).should.be.instanceof(Number)
    })
  })
})