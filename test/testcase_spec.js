const should = require('should')
const mocha = require('mocha')
const Testcase = require('./../lib/Testcase')


describe('Testing testcase', function() {
  describe('Parsing M', function() {
    
    it('Should throw err on M equals to 0', function(){
      should( () => new Testcase('', 1, 0)).throw('M must be between 1 and 1000')
    })

    it('Should throw err on M equals to 1001', function(){
      should( () => new Testcase('', 1, 1001)).throw('M must be between 1 and 1000')
    })

    it('Should throw err on M are different than length operations', function(){
      should( () => new Testcase([{action: 'UPDATE', props:{}}], 1, 2)).throw('Operations length must equals to M')
    })

    it('Should pass if M are equals than length operations', function(){
      new Testcase([{action: 'UPDATE', props:{}}], 1, 1).should.be.instanceof(Object)
    })

  })
})