import s from './main.sass'
import Vue from 'vue/dist/vue'
import Editor from './components/Editor.vue'
import store from './utils/store'
import api from './utils/api'
  

const app = new Vue ({
  el: '#app',
  
  data: {
    message: 'test',
    shared: store.data,
    error: '',
    modal: false
  },

  components: {
    'editor': Editor
  },

  methods: {
    async run(){
      this.shared.results = ''
      
      try{
        const response = await api.post('', {input: this.shared.input})
        this.shared.results = response.data.results.join('<br>') 
      }catch(error){
        this.error = error.response.data
        this.showDialog()
      }
    },

    showDialog(){
      this.modal = true
    },

    hideDialog(){
      this.modal = false
    },

    clear(){
      this.shared.input = ''
      this.shared.results = ''
    }
  }
})