const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {

  entry: ['babel-polyfill', './src/main.js'],

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/, 
        exclude: /(node_modules)/, 
        loader: 'babel-loader', 
      }, {
        test: /\.sass$/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader"},
          {loader: "sass-loader", options: {includePaths: ["./node_modules/", "./bower_components/"]}},
        ]
      }, {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader?name=fonts/[name].[ext]'
      },{
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: 'vue-style-loader!css-loader!sass-loader', 
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          }
        }
      }
    ]
  },

  plugins: [new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'src/index.html'
  })],

  devServer: {
    proxy: {
      "/": "http://localhost:3000"
    }
  }
}