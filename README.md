# Cube Summation Client
Code solution for hackerrank [Cube Summation challengue](https://www.hackerrank.com/challenges/cube-summation)

## Backend
The backend was wrote in nodejs, using express for routing, Please run it with **node 7.4+** 

## Frontend
Implements Vue as view render engine, Babel transpilator, Sass preprocessor, bulma CSS framework and axios HTTP client

## Develop Tools
Webpack for build tasks, nodemon for server watch and mocha for testing


# Backend structure

## Api
Contains controllers and routing logic

## Api/Controller
Takes care of parse all input data, convert text to parametters

## Lib
Contains reusable classes

## Lib/Matrix
Has 3D matrix methods calculation 

## Lib/Testcase
Manage the matrix creation and queries result


# Instructions
How to install
```
npm install
```
___

How to run develop server
```
npm run dev
```
then open http://localhost:8080/
___

How to run test
```
npm run test
```

