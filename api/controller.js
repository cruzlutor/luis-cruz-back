const Testcase = require('./../lib/Testcase')

/**
 * Convert input text to array
 * @param  {string} input User string input operations
 * @return {array}       list of all operations and instructions
 */
function parseInput(input){
  return input.split('\n')
}

/**
 * Get the number of cases
 * @param  {string} input User string input operations
 * @return {number}       number of cases
 */
function parseCases(input){
  if(!input[0]) throw new Error('Invalid input format')
  return parseInt(input[0].trim())
}

/**
 * Parse N,M values
 * @param  {string} input Line that contains the N,M instructions
 * @return {array}       N,M values
 */
function parseInstructions(input){
  if(!input) throw new Error('Invalid input format')
  return input.split(' ').map( part => parseInt(part))
}

/**
 * Get operations of current case
 * 
 * @param  {string} input User string input operations
 * @param  {number} start Start line of operations
 * @param  {number} lines Number of operations
 * 
 * @return {array}       List of operations for the current case
 */
function getOptions(input, start, lines){
  let operations = []
  for(let i = start; i < start + lines; i++){
    operations.push(parseOperation(input[i]))
  }
  return operations
}

/**
 * Parse operation string to get actions and params of each operation
 * @param  {string} input Line with operation detail
 * @return {object}       Object of arrays with all case operations
 */
function parseOperation(input){
  if(!input || !input.trim().match(/^U|^Q/)) throw new Error('Invalid operation, M and operations length must be equals')

  let parts = input.trim().split(' ')
  return {
    action: parts[0],
    params: parts.slice(1).map( part => parseInt(part))
  }
}

/**
 * Create new testcase object
 * 
 * @param  {string} input User string input operations
 * @param  {number} line  Line when the case starts
 * @param  {array} cases list of all cases created for this request
 * 
 * @return {none}       
 */
function createCase(input, line, cases){
  let instructions = parseInstructions(input[line])
  let start = line + 1  
  let next = start + instructions[1]
  let operations = getOptions(input, start, instructions[1])
  let testcase = new Testcase(operations, ...instructions)
  cases.push(testcase)
  if(input[next]) createCase(input, next, cases)
}

/**
 * Main controller function
 * 
 * @param  {object} req Express req object
 * @param  {object} res Express res object
 * 
 * @return {object}     Express formatted response
 */
function main(req, res){
  let cases = []
  let results = []
  let input = parseInput(req.body.input || '')
  let t = parseCases(input)

  if(t < 1 || t > 50){
    throw new Error('T must be between 1 and 50')
  }

  createCase(input, 1, cases)

  if(t != cases.length){
    throw new Error('T and cases must be equals')
  }

  for(let element in cases){
    results = results.concat(cases[element].run())
  }
  return res.json({results : results})
}

module.exports = {
  main
}