const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()


app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static('dist'))
app.get('/', (req, res) => res.sendFile('index.html'))
app.use('/api', require('./api/routes'))

app.listen(3000, () => {
  console.log('Server listening on 3000')
})