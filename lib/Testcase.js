const Matrix = require('./Matrix')


class Testcase {

  constructor(operations = [], n, m){

    if(m < 1 || m > 1000){
      throw new Error('M must be between 1 and 1000')
    } 

    if(m != operations.length){
      throw new Error('Operations length must equals to M')
    } 

    /**
     * List of operations to appli
     * @type {array}
     */
    this.operations = operations

    /**
     * Limit of operations 
     * @type {number}
     */
    this.m = m

    /**
     * The amount of matirx blocks
     * @type {number}
     */
    this.n = n

    /**
     * Matrix object for this test case
     * @type {matrix}
     */
    this.matrix = new Matrix()

    /**
     * Matrix query results
     * @type {array}
     */
    this.results = []
  }

  /**
   * Run matrix operations
   * @return {array} List of queries result
   */
  run(){
    this.matrix.build(this.n)
    for(let o in this.operations){
      if(!this.operations[o].action){
        throw new Error('Operation action is a must')
      } 

      if(!this.operations[o].params){
        throw new Error('Operation params is a must')
      } 

      if(this.operations[o].action == 'UPDATE'){
        this.matrix.update(...this.operations[o].params)
      }

      if(this.operations[o].action == 'QUERY'){
        this.results.push(this.matrix.query(...this.operations[o].params))
      }
    }

    return this.results
  }
}

module.exports = Testcase