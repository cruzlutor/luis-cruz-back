class Matrix {

  constructor(){

    /**
     * Matrix data
     * @type {array}
     */
    this.matrix = []

    /**
     * N, number of blocks in this matrix
     * @type {number}
     */
    this.n = 0
  }

  /**
   * Build Matrix blocks
   * 
   * @param  {number} n n position for array
   * 
   * @return {object}   Current matrix
   */
  build(n){
    if(!n || n < 1 || n > 100){
      throw new Error('N must be between 0 and 100')
    }

    this.n = n

    for(let x = 1; x <= this.n; x++){
      this.matrix[x] = []
      for(let y = 1; y <= this.n; y++){
        this.matrix[x][y] = []
        for(let z = 1; z <= this.n; z++){
          this.matrix[x][y][z] = 0
        }
      }
    }
    
    return this.matrix
  }

  /**
   * Update specific matrix block
   * 
   * @param  {number} x X matrix position
   * @param  {number} y Y matrix position
   * @param  {number} z X matrix position
   * @param  {number} w New block value
   * 
   * @return {object}   Current matrix
   */
  update(x, y, z, w){
    if(!x || x < 1 || x > this.n){
      throw new Error('X must be between 0 and N')
    }

    if(!y || y < 1 || y > this.n){
      throw new Error('Y must be between 0 and N')
    }

    if(!z || z < 1 || z > this.n){
      throw new Error('Z must be between 0 and N')
    }

    if(!w || w < Math.pow(-10, 9) || w > Math.pow(10, 9)){
      throw new Error('W must be between -10^9 and 10^9')
    }

    this.matrix[x][y][z] = w

    return this.matrix
  }

  /**
   * Calculate the sum of blocks between coordinates
   * 
   * @param  {number} x1 X start coordinate
   * @param  {number} y1 Y start coordinate
   * @param  {number} z1 Z start coordinate
   * @param  {number} x2 X end coordinate
   * @param  {number} y2 Y end coordinate
   * @param  {number} z2 Z end coordinate
   * 
   * @return {number}    Coordinates sum result
   */
  query(x1, y1, z1, x2, y2, z2){
    if(!x1 || x1 < 1 || x1 > this.n){
      throw new Error('X1 must be between 0 and N')
    }

    if(!y1 || y1 < 1 || y1 > this.n){
      throw new Error('Y1 must be between 0 and N')
    }

    if(!z1 || z1 < 1 || z1 > this.n){
      throw new Error('Z1 must be between 0 and N')
    }

    if(!x2 || x2 < 1 || x2 > this.n){
      throw new Error('X2 must be between 0 and N')
    }
    
    if(!y2 || y2 < 1 || y2 > this.n){ 
      throw new Error('Y2 must be between 0 and N')
    }

    if(!z2 || z2 < 1 || z2 > this.n){
      throw new Error('Z2 must be between 0 and N')
    }

    if(x2 < x1){
      throw new Error('X2 must be higher or equal than X1')
    }

    if(y2 < y1){
      throw new Error('Y2 must be higher or equal than Y1')
    }
    
    if(z2 < z1){
      throw new Error('Z2 must be higher or equal than Z1')
    }
    
    let sum = 0
    for(let x = x1; x <= x2; x++){
      for(let y = y1; y <= y2; y++){
        for(let z = z1; z <= z2; z++){
          sum += this.matrix[x][y][z]
        }
      }
    }

    return sum
  }
}

module.exports = Matrix